# -*- coding: utf-8 -*-
"""
Created on Thu Sep 13 22:48:01 2018

@author: Robert
"""
import os
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.image as mimg
from keras.regularizers import l2
from os import listdir, makedirs, getcwd, remove
from os.path import isfile, join, abspath, exists, isdir, expanduser
from PIL import Image
from keras.models import Sequential, Model
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.preprocessing.image import ImageDataGenerator,load_img, img_to_array
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Activation, Dropout, Input, Flatten, BatchNormalization
from keras.models import Model
from keras.optimizers import Adam
from keras.optimizers import SGD
from keras.applications import InceptionV3
from sklearn.metrics import classification_report, confusion_matrix

def get_model():
    # Dohvaćanje baznog modela InceptionV3
    base_model = InceptionV3(include_top=False, input_shape=(300,300,3))
    # Sprječava učenje layera Inceptionv3 pretreniranog modela
    for layer in base_model.layers:
        layer.trainable = False
    
    # Dodavanje novih slojeva na InceptionV3 model
    x = Flatten()(base_model.output)
    x = Dense(100, name='Dense_layer1',use_bias=False)(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = Dense(100, name='Dense_layer2',use_bias=False)(x)
    x = BatchNormalization()(x)
    x = Activation("relu")(x)
    x = Dense(5, activation='softmax', name='classification_layer')(x)
    
    model = Model(inputs=base_model.input, outputs=x)
    # Kompajliranje modela koristeći SGD optimizer
   # opt = SGD(lr=1e-3, momentum=0.9)
    opt=Adam(lr=5e-3, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    #Sažetak
    model.summary()
    return model



#Učitavanje i prikaz svih podataka sadržanih u datasetu
flowers_path = "flowers/"
flower_types = os.listdir(flowers_path)
print("Types of flowers found: ", len(flower_types))
print("Categories of flowers: ", flower_types)

# Kreiranje liste cvijeca
flowers = []

for species in flower_types:
    # Dohvaćanje svih imena datoteka
    all_flowers = os.listdir(flowers_path + species)
    # Dodavanje imena na listu
    for flower in all_flowers:
        flowers.append((species, str(flowers_path + species) + '/' + flower))

# Generiranje dataframea     
flowers = pd.DataFrame(data=flowers, columns=['category', 'image'], index=None)
flowers.head()

# Ispis broja slika po kategoriji dataseta
print("Total number of flowers in the dataset: ", len(flowers))
fl_count = flowers['category'].value_counts()
print("Flowers in each category: ")
print(fl_count)

# Prikaz barplot dataseta
plt.figure(figsize=(12,8))
sns.barplot(x=fl_count.index, y=fl_count.values)
plt.title("Flowers count for each category", fontsize=16)
plt.xlabel("Category", fontsize=14)
plt.ylabel("Count", fontsize=14)
plt.show()


#Vizualizacija dataseta
# Lista za dohvaćanje random slika iz dataseta
random_samples = []

# Dohvaćanje 4 uzorka iz svake kategorije 
for category in fl_count.index:
    samples = flowers['image'][flowers['category'] == category].sample(4).values
    for sample in samples:
        random_samples.append(sample)

# Prikaz uzoraka
f, ax = plt.subplots(5,4, figsize=(15,10))
for i,sample in enumerate(random_samples):
    ax[i//4, i%4].imshow(mimg.imread(random_samples[i]))
    ax[i//4, i%4].axis('off')
plt.show()   




# Izgradnja generatora skupova podataka

batch_size = 50
# Postavke za generator train skupa podataka
train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

# Postavka za generator test skupa podataka
test_datagen = ImageDataGenerator(rescale=1./255)

#Train generator koji dohvaća slike u subfolderima train skupa 
train_generator = train_datagen.flow_from_directory(
        'organizeflowersdataset/change_flowers/train',
        target_size=(300, 300),  # cropa sve slike na 150x150
        batch_size=batch_size,
        color_mode='rgb',
        class_mode='categorical',# više klasa
        shuffle=True)  

#Validation generator koji dohvaća slike u subfolderima validation skupa
validation_generator = test_datagen.flow_from_directory(
        'organizeflowersdataset/change_flowers/validation',
        target_size=(300,300),
        batch_size=batch_size,
        color_mode='rgb',
        class_mode='categorical',
        shuffle=True)
#Test generator koji dohvaća slike u subfolderima validation skupa
test_generator=test_datagen.flow_from_directory(
        'organizeflowersdataset/change_flowers/test',
        target_size=(300,300),
        color_mode='rgb',
        batch_size=batch_size,
        shuffle=False,
        class_mode='categorical')


# Dohvaćanje modela (VG
# G16 + custom layeri)
model = get_model()

# Fitanje train i validation generatora na model 
model.fit_generator(
        train_generator,
        steps_per_epoch=4000 // batch_size,
        epochs=10,
        validation_data=validation_generator,
        validation_steps=911 // batch_size)

#evaluacija modela
Y_pred = model.predict_generator(test_generator, 911 // batch_size+1)
y_pred = np.argmax(Y_pred, axis=1)
print('Confusion Matrix')
print(confusion_matrix(test_generator.classes, y_pred))
print('Classification Report')
target_names = ['daisy', 'dandelion', 'rose','sunflower','tulip']
print(classification_report(test_generator.classes, y_pred, target_names=target_names))
